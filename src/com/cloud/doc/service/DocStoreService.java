package com.cloud.doc.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloud.attach.Attach;
import com.cloud.doc.model.Directory;
import com.cloud.doc.model.DocFile;
import com.cloud.doc.util.DocUtil;
import com.cloud.platform.Constants;
import com.cloud.platform.IDao;
import com.cloud.platform.StringUtil;
import com.cloud.platform.Tree;

@Service
public class DocStoreService {

	@Autowired
	private IDao dao;
	
	/**
	 * upload files
	 * 
	 * @param parentId
	 * @param attachIds
	 */
	public void uploadFiles(String parentId, String attachIds) {
		
		if(StringUtil.isNullOrEmpty(attachIds)) {
			return;
		}
		
		for(String attachId : attachIds.split(",")) {
			attachId = attachId.trim();
			Attach attach = (Attach) dao.getObject(Attach.class, attachId);
			
			// save file
			DocFile file = new DocFile();
			file.setId(Constants.getID());
			file.setName(attach.getFileName());
			file.setParentId(parentId);
			file.setCreateTime(new Date());
			file.setCreator(Constants.getLoginUserId());
			
			dao.saveObject(file);
			
			// update attach entity relate
			attach.setEntityId(file.getId());
			
			dao.saveObject(attach);
		}
	}
	
	/**
	 * get dir by id
	 * 
	 * @param dirId
	 * @return
	 */
	public Directory getDirectory(String dirId) {
		
		if(StringUtil.isNullOrEmpty(dirId)) {
			return new Directory();
		}
		
		return (Directory) dao.getObject(Directory.class, dirId);
	}

	/**
	 * remove dir or file
	 * 
	 * @param docId
	 * @param isDir
	 */
	public void remove(String docId, boolean isDir) {
		
		if(isDir) {
			dao.removeById(Directory.class, docId);
		} else {
			dao.removeById(DocFile.class, docId);
		}
	}
	
	/**
	 * rename dir or file
	 * 
	 * @param docId
	 * @param newName
	 */
	public void rename(String docId, String newName, boolean isDir) {
		
		if(StringUtil.isNullOrEmpty(docId, newName)) {
			return;
		}
		
		Tree doc = null;
		
		if(isDir) {
			doc = (Tree) dao.getObject(Directory.class, docId);
		} else {
			doc = (Tree) dao.getObject(DocFile.class, docId);
		}
		
		doc.setName(newName);
		
		dao.saveObject(doc);
	}
	
	/**
	 * search dirs
	 * 
	 * @param parentId
	 * @return
	 */
	public List<Directory> searchDirs(String parentId) {
		
		if(StringUtil.isNullOrEmpty(parentId)) {
			return dao.getAllByHql("from Directory where parentId = '' or parentId is null order by name");
		} else {
			return dao.getAllByHql("from Directory where parentId = ? order by name", parentId);
		}
	}
	
	/**
	 * search files
	 * 
	 * @param parentId
	 * @return
	 */
	public List<DocFile> searchFiles(String parentId) {
		
		List<Object[]> list = null;
		
		if(StringUtil.isNullOrEmpty(parentId)) {
			list = dao.getAllByHql("select f,a from DocFile f,Attach a where f.id = a.entityId and (f.parentId = '' or f.parentId is null) order by f.name");
		} else {
			list = dao.getAllByHql("select f,a from DocFile f,Attach a where f.id = a.entityId and f.parentId = ? order by f.name", parentId);
		}
		
		// combine file attach
		List newList = new ArrayList();
		
		for(Object[] obj : list) {
			DocFile file = (DocFile) (obj[0]);
			Attach attach = (Attach) (obj[1]);
			
			file.setAttach(attach);
			newList.add(file);
		}
		
		return newList;
	}
	
	/**
	 * create new dir
	 * 
	 * @return
	 */
	public String createDir(String parentId) {
		String id = Constants.getID();
		
		Directory dir = new Directory(id);
		dir.setName(DocUtil.DEFAULT_DIR_NAME);
		dir.setParentId(parentId);
		
		dao.saveObject(dir);
		return id;
	}
}
