package com.cloud.platform;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletContext;

import org.springframework.core.io.Resource;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.context.support.ServletContextResource;

import com.cloud.system.model.SystemConfig;

public class DocConstants implements ServletContextAware {

	private static ServletContext servletContext;
	
	/**
	 * inject servlet context
	 */
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	public static SystemConfig systemConfig;
	
	/**
	 * check if is text file
	 * 
	 * @param extend
	 * @return
	 * @throws IOException 
	 */
	public static boolean isText(String extend) throws IOException {
	
		Resource propRsr = new ServletContextResource(servletContext, "/WEB-INF/text.properties");
		
		Properties prop = new Properties();
		prop.load(propRsr.getInputStream());
		
		return prop.containsValue(extend);
	}
	
	/**
	 * get system config
	 * 
	 * @return
	 */
	public static SystemConfig getSystemConfig() {
		
		if(systemConfig == null) {
			IDao dao = (IDao) SpringUtil.getBean("dao");
			List<SystemConfig> list = dao.getAllByHql("from SystemConfig");
			
			systemConfig = list.get(0);
		}
		
		return systemConfig;
	}
}
