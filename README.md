PPM Doc 文档管理系统
=======

项目主页： http://www.ppm123.cn

免费下载： http://ppm123.cn/pages/doc/detail.php

简介：

PPM Doc文档管理系统是国内第一款开源的企业文档管理系统，旨在帮助企业摆脱VSS等粗糙的文档管理软件，方便、快捷管理并协同企业文档！

1. 文档仓库 -- 简单清爽的界面风格，Windows文件目录式操作习惯

2. 版本控制 -- 文档检入检出，控制文档版本，部门内文档协同

3. 版本比较 -- 对比文档的不同版本，直观查看不同版本间差异

4. 在线浏览 -- 在线浏览文本、图片、Office文件、PDF等文档

5. 全文检索 -- 全文检索文本型文件及Office文件内部内容

6. 在线 PPT -- 不用拷贝演示文件，输入文档仓库网址在线演示PPT

版本：

PPM Doc 开源文档管理系统 v1.0 第1个版本于2013年9月14号正式发布，欢迎大家免费下载使用。
 
技术体系
=======

前端：

主 -- JQuery + Bootstrap + JQueryUI(bootstrap theme) + iCheck + uploadify + FancyBox

附 -- JQuery自定义插件 + 下拉框插件 + Bootstrap TAB插件 + 表单校验插件

后端：

主 -- SpringMVC + Sitemesh + Hibernate(注解) + Spring(Ioc) + Spring Security + JSP2 TagDir + log4j

附 -- 反射 + 实体增删改查自动处理框架

源码使用
=======

查看PPM Doc的源码并运行起来十分的简单，并且源码提供了非常详细的注释

1. 首先PPM的所有开源项目都是基于open-platform平台进行开发，请先下载部署PPM平台项目

2. 下载zip包，解压

3. Eclipse引入已存在项目，选择刚才解压的文件夹

4. 关联open-platform平台项目，右键open-doc项目Build Path，在Projects中添加open-platform平台项目

5. PPM Doc采用内存数据库derby，所以只需将解压的文件夹下的db-doc文件夹放到tomcat安装目录的bin文件夹下即可，无需安装数据库

6. 打开WebRoot/WEB-INF/jdbc.properties，修改数据库配置，使用 4 步骤的derby数据库的话将url=jdbc:derby:D:/derby/bin/db-doc改为url=jdbc:derby:db-doc

7. 部署，运行，默认用户名admin，密码1
